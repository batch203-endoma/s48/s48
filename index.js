console.log(`Hello World`);

//fetch()
// this is a method in JavaScript that is used to send request in the server and load the information(response from the server) in the webpages. (JSON format)


// syntax:
// fetch ("urlAPI", {options})
// url - the url which the request is to be made.
//options - an array of properties, optional parameter.


/*
    Endpoint of the API:
    GET and POST:
    https://sheltered-hamlet-48007.herokuapp.com/movies


    update and Delete:
    https://sheltered-hamlet-48007.herokuapp.com/movies/:id

    //GET - doesnt need any options
    Post & put - method, body, and content-Type
    DElete - Method.
*/

//Get all data.

let fetchMovies = () => {
  fetch("https://sheltered-hamlet-48007.herokuapp.com/movies")
    .then(response => response.json())
    .then(data => showPosts(data));

}

fetchMovies();


// show fetch data in the document (HTML web page)
const showPosts = (movies) => {
  console.log(movies)

  //create a variable that will contain all the movies.
  let movieEntries = "";
  movies.forEach((movie) => {
    //onclick() is an event occurs when the user click on an element
    // this allows us to execute a JavaScript's function when an element get clicked
    movieEntries += `
  			<div id="movie-${movie._id}">
  				<h3 id="movie-title-${movie._id}">${movie.title}</h3>
  				<p id="movie-desc-${movie._id}">${movie.description}</p>
  				<button onclick="editMovie('${movie._id}')">Edit</button>
  				<button onclick="deleteMovie('${movie._id}')">Delete</button>
  			</div>
  		`;
  });

  // console.log(movieEntries);
  document.querySelector("#div-movie-entries").innerHTML = movieEntries;
}
//
// Add movie data


document.querySelector("#form-add-movie").addEventListener("submit", (e) => {
  e.preventDefault();
  let title = document.querySelector("#txt-title").value
  let description = document.querySelector("#txt-desc").value
  fetch("https://sheltered-hamlet-48007.herokuapp.com/movies", {
      method: "POST",
      body: JSON.stringify({
        title: title,
        description: description
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
    .then(response => response.json())
    .then(data => {
      console.log(data);
      alert("Successfully added.")
      document.querySelector("#txt-title").value = null;
      document.querySelector("#txt-desc").value = null;

      // to show new movies
      fetchMovies();
      //lastName movie; sample description
    })
})



const editMovie = (id) => {
  console.log(id);
  let title = document.querySelector(`#movie-title-${id}`).innerHTML;
  let description = document.querySelector(`#movie-desc-${id}`).innerHTML;

  console.log(title);
  console.log(description);

  //Pass the id, title, and description in the edit form and enable the update button.
  document.querySelector("#txt-edit-id").value = id;
  document.querySelector("#txt-edit-title").value = title;
  document.querySelector("#txt-edit-desc").value = description;
  document.querySelector("#btn-submit-update").removeAttribute("disabled");
}

//update a movie
//this will trigger

document.querySelector("#form-edit-movie").addEventListener("submit", (e) => {
  e.preventDefault();
  let id = document.querySelector("#txt-edit-id").value
  fetch(`https://sheltered-hamlet-48007.herokuapp.com/movies/${id}`, {
      method: "PUT",
      body: JSON.stringify({
        title: document.querySelector("#txt-edit-title").value,
        description: document.querySelector("#txt-edit-desc").value

      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
    .then(response => response.json())
    .then(data => {
      console.log(data);
      alert("Succesfully updated.");
      document.querySelector("#txt-edit-id").value = null;
      document.querySelector("#txt-edit-title").value = null;
      document.querySelector("#txt-edit-desc").value = null;
      document.querySelector("#btn-submit-update").setAttribute("disabled", true);
    })
})

/*
   1. Add a new function in index.js called deleteMovie().
    2. This function should be able to receive the id number of the post:
        - Create a fetch request to delete a document by its id.
    3. Create a git repository named S49.
    4. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
   5.  Add the link in Boodle.

*/





const deleteMovie = (id) => {
  let element = document.querySelector(`#movie-${id}`);
  console.log(element);

  fetch(`https://sheltered-hamlet-48007.herokuapp.com/movies/${id}`, {
      method: "DELETE"
    })
    .then(response => response.json())
    .then(data => {
      console.log(data);
      alert("Successfully deleted")
    })
  fetchMovies()
}











//
